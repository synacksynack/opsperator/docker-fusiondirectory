FROM opsperator/php

# FusionDirectory image for OpenShift Origin

ARG DO_UPGRADE=

LABEL io.k8s.description="FusionDirectory LDAP Manager" \
      io.k8s.display-name="FusionDirectory 1.3" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="fusion,fusiondirectory,fusiondirectory1" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-fusiondirectory" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.3"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install FusionDirectory dependencies" \
    && if test `uname -m` = aarch64; then \
	sed 's|amd64|arm64|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-cas php-cli php-common php-curl php-gd php-imagick \
	    php-imap php-ldap php-mbstring php-recode php php-fpdf php-xml; do \
	    echo "/." >/var/lib/dpkg/info/$dep:arm64.list; \
	done; \
    elif test `uname -m` = armv7l; then \
	sed 's|amd64|armhf|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-cas php-cli php-common php-curl php-gd php-imagick \
	    php-imap php-ldap php-mbstring php-recode php php-fpdf php-xml; do \
	    echo "/." >/var/lib/dpkg/info/$dep:armhf.list; \
	done; \
    else \
	cat /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-cas php-cli php-common php-curl php-gd php-imagick \
	    php-imap php-ldap php-mbstring php-recode php php-fpdf php-xml; do \
	    echo "/." >/var/lib/dpkg/info/$dep:amd64.list; \
	done; \
    fi \
    && a2enmod auth_openidc \
    && apt-get -y install --no-install-recommends smarty3 gettext libpng16-16 \
	ldap-utils openssl libjpeg62-turbo libwebp6 libgd3 \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y install --no-install-recommends libicu-dev gnupg2 wget \
	apt-transport-https libjpeg62-turbo-dev libfreetype6-dev libwebp-dev \
	libcurl4-openssl-dev libmagickwand-dev libc-client-dev libkrb5-dev \
	libpng-dev libldap2-dev libonig-dev libxml2-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install mbstring intl gd ldap curl imap gettext xml \
    && pecl install imagick-3.7.0 \
    && docker-php-ext-enable imagick xml \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && echo "# Install FusionDirectory" \
    && apt-key add /FD-archive-key \
    && mv /fd.list /etc/apt/sources.list.d/fusiondirectory.list \
    && apt-get update \
    && apt-get install -y fusiondirectory fusiondirectory-plugin-dsa \
	fusiondirectory-plugin-ppolicy fusiondirectory-plugin-ldapdump \
	fusiondirectory-plugin-ldapmanager fusiondirectory-plugin-mail \
	fusiondirectory-smarty3-acl-render \
    && echo "# Apply PHP7.4 Patches" \
    && ( \
	cd / \
	&& for p in /*.patch; \
	    do patch -p0 <$p; \
	done; \
    ) \
    && echo "# Fixing permissions" \
    && for dir in /etc/fusiondirectory /var/cache/fusiondirectory \
	/var/spool/fusiondirectory; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "# Cleaning up" \
    && a2dismod perl \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/FD-archive-key /usr/src/php.tar.xz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-fusion.sh"]
USER 1001
