# k8s FusionDirectory

FusionDirectory image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Depends on a FusionDirectory capable OpenLAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Build with:

```
$ make build
```

Test with:

```
$ make syndemo
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

FIXME: fpdf

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name            |    Description             | Default                                                     | Inherited From    |
| :-------------------------- | -------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`            | Fusion VirtualHost         | `fusiondirectory.${OPENLDAP_DOMAIN}`                        | opsperator/apache |
|  `APACHE_HTTP_PORT`         | Fusion Directory HTTP Port | `8080`                                                      | opsperator/apache |
|  `AUTH_METHOD`              | Fusion Auth Method         | `internal` (built-in LDAP login), set `oidc` for LLNG       |                   |
|  `DEBUG_LEVEL`              | Fusion Debug Level         | `0`                                                         |                   |
|  `DISPLAY_ERRORS`           | Display Fusion Errors      | `FALSE`                                                     |                   |
|  `OIDC_CALLBACK_URL`        | OpenID Callback Path       | `/oauth2/callback`                                          |                   |
|  `OIDC_CLIENT_ID`           | OpenID Client ID           | `changeme`                                                  |                   |
|  `OIDC_CLIENT_SECRET`       | OpenID Client Secret       | `secret`                                                    |                   |
|  `OIDC_CRYPTO_SECRET`       | OpenID Crypto Secret       | `secret`                                                    |                   |
|  `OIDC_META_URL`            | OpenID Meta Path           | `/.well-known/openid-configuration`                         |                   |
|  `OIDC_PORTAL`              | OpenID Portal              | `https://auth.$OPENLDAP_DOMAIN`                             |                   |
|  `OIDC_SKIP_TLS_VERIFY`     | OpenID Skip TLS Verify     | undef                                                       |                   |
|  `OIDC_TOKEN_ENDPOINT_AUTH` | OpenID Auth Method         | `client_secret_basic`                                       |                   |
|  `ONLY_TRUST_KUBE_CA`       | Don't trust base image CAs | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`            | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`  | OpenLDAP Bind DN Prefix    | `cn=fusiondirectory,ou=services`                            | opsperator/apache |
|  `OPENLDAP_BIND_PW`         | OpenLDAP Bind Password     | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`          | OpenLDAP Domain Name       | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`            | OpenLDAP Backend Address   | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`            | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`           | OpenLDAP Proto             | `ldap`                                                      | opsperator/apache |
|  `PHP_ERRORS_LOG`           | PHP Errors Logs Output     | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`   | PHP Max Execution Time     | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`     | PHP Max File Uploads      .| `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`        | PHP Max Post Size        . | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`  | PHP Max Upload File Size   | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`         | PHP Memory Limit           | `-1` (no limitation)                                        | opsperator/php    |
|  `PUBLIC_PROTO`             | Apache Public Proto        | `http`                                                      | opsperator/apache |

The following table details the possible debug levels.

| Debug Level | Description           |
| ----------- | --------------------- |
|  0          | Enable no debugging   |
|  1          | Trace function calls  |
|  2          | LDAP Debugs           |
|  4          | Database              |
|  8          | Shell                 |
|  16         | Post                  |
|  32         | Session               |
|  64         | Configuration         |
|  128        | ACL                   |
|  256        | Argonaut              |
|  512        | Mail                  |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
