#!/bin/sh

if test "$DEBUG"; then
    set -x
    DEBUG_LEVEL=183
    DISPLAY_ERRORS=TRUE
    #ldap+db+trace+post+session+acl => 2+4+1+16+32+128
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
AUTH_METHOD=${AUTH_METHOD:-internal}
DEBUG_LEVEL=${DEBUG_LEVEL:-0}
DISPLAY_ERRORS=FALSE
OIDC_CALLBACK_URL=${OIDC_CALLBACK_URL:-/oauth2/callback}
OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-changeme}
OIDC_CLIENT_SECRET=${OIDC_CLIENT_SECRET:-secret}
OIDC_CRYPTO_SECRET=${OIDC_CRYPTO_SECRET:-secret}
OIDC_META_URL=${OIDC_META_URL:-/.well-known/openid-configuration}
OIDC_TOKEN_ENDPOINT_AUTH=${OIDC_TOKEN_ENDPOINT_AUTH:-client_secret_basic}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=fusiondirectory,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
USE_TLS=FALSE
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=fusiondirectory.$OPENLDAP_DOMAIN
fi
if test "$AUTH_METHOD" = oidc -a -z "$OIDC_PORTAL"; then
    OIDC_PORTAL=$PUBLIC_PROTO://auth.$OPENLDAP_DOMAIN
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
if ! test "$SSL_INCLUDE" = no-ssl; then
    USE_TLS=TRUE
fi

sed -i 's|.*pcre.backtrack_limit.*|pcre.backtrack_limit=10000000|g' \
    /usr/local/etc/php/php.ini

echo "Install Fusion Service Configuration"
cat <<EOF >/etc/fusiondirectory/fusiondirectory.conf
<?xml version="1.0"?>
<conf>
  <main default="default"
        logging="TRUE"
        displayErrors="$DISPLAY_ERRORS"
        forceSSL="$USE_TLS"
        templateCompileDirectory="/var/spool/fusiondirectory/"
        debugLevel="$DEBUG_LEVEL" >
    <location name="default">
      <referral URI="$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/$OPENLDAP_BASE"
                adminDn="$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE"
                adminPassword="$OPENLDAP_BIND_PW" />
    </location>
  </main>
</conf>
EOF
chmod 640 /etc/fusiondirectory/fusiondirectory.conf

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo "Generates FusionDirectory VirtualHost Configuration"
    if test "$AUTH_METHOD" = oidc; then
	if ! echo "$OIDC_CALLBACK_URL" | grep ^/ >/dev/null; then
	    OIDC_CALLBACK_URL=/$OIDC_CALLBACK_URL
	fi
	CALLBACK_ROOT_SUB_COUNT=`echo $OIDC_CALLBACK_URL | awk -F/ '{print NF}'`
	CALLBACK_ROOT_SUB_COUNT=`expr $CALLBACK_ROOT_SUB_COUNT - 2 2>/dev/null`
	if ! test "$CALLBACK_ROOT_SUB_COUNT" -ge 2; then
	    CALLBACK_ROOT_SUB_COUNT=2
	fi
	CALLBACK_ROOT_URL=`echo $OIDC_CALLBACK_URL | cut -d/ -f 2-$CALLBACK_ROOT_SUB_COUNT`
	sed -e "s|CALLBACK_ROOT_URL|$CALLBACK_ROOT_URL|g" \
	    -e "s|FUSION_HOSTNAME|$APACHE_DOMAIN|g" \
	    -e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
	    -e "s|OIDC_CALLBACK_URL|$OIDC_CALLBACK_URL|g" \
	    -e "s|OIDC_CLIENT_ID|$OIDC_CLIENT_ID|g" \
	    -e "s|OIDC_CLIENT_SECRET|$OIDC_CLIENT_SECRET|g" \
	    -e "s|OIDC_CRYPTO_SECRET|$OIDC_CRYPTO_SECRET|g" \
	    -e "s|OIDC_META_URL|$OIDC_META_URL|g" \
	    -e "s|OIDC_PORTAL|$OIDC_PORTAL|g" \
	    -e "s|OIDC_TOKEN_ENDPOINT_AUTH|$OIDC_TOKEN_ENDPOINT_AUTH|g" \
	    -e "s|PUBLIC_PROTO|$PUBLIC_PROTO|g" \
	    -e "s|SSL_TOGGLE_INCLUDE|$SSL_INCLUDE.conf|g" \
	    /vhost-oidc.conf
    else
	rm -f /etc/apache2/mods-enabled/*openid*
	sed -e "s|FUSION_HOSTNAME|$APACHE_DOMAIN|g" \
	    -e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
	    -e "s|SSL_TOGGLE_INCLUDE|$SSL_INCLUDE.conf|g" \
	    /vhost.conf
    fi >/etc/apache2/sites-enabled/003-vhosts.conf
fi
if test "$AUTH_METHOD" = oidc; then
    if test "$PUBLIC_PROTO" = https -a "$OIDC_SKIP_TLS_VERIFY"; then
	sed -i \
	    -e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer Off|' \
	    -e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer Off|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    else
	sed -i \
	    -e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer On|' \
	    -e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer On|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    fi
    sed -i \
	-e 's|^#*[ ]*OIDCPassClaimsAs.*|OIDCPassClaimsAs environment|' \
	-e 's|^#*[ ]*OIDCProviderAuthRequestMethod.*|OIDCProviderAuthRequestMethod GET|' \
	-e 's|^#*[ ]*OIDCUserInfoTokenMethod.*|OIDCUserInfoTokenMethod authz_header|' \
	-e 's|^#*[ ]*OIDCSessionCookieChunkSize.*|OIDCSessionCookieChunkSize 4000|' \
	-e 's|^#*[ ]*OIDCSessionType.*|OIDCSessionType client-cookie|' \
	-e 's|^#*[ ]*OIDCStripCookies.*|OIDCStripCookies mod_auth_openidc_session mod_auth_openidc_session_chunks mod_auth_openidc_session_0 mod_auth_openidc_session_1|' \
	/etc/apache2/mods-enabled/auth_openidc.conf
fi

export RESET_TLS=false

. /run-apache.sh
